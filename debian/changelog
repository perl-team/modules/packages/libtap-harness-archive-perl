libtap-harness-archive-perl (0.18-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster (oldstable)

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 19 Nov 2022 15:20:18 +0000

libtap-harness-archive-perl (0.18-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Set debhelper-compat version in Build-Depends.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 14 Oct 2022 13:51:05 +0100

libtap-harness-archive-perl (0.18-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Alex Muntada ]
  * Remove inactive pkg-perl members from Uploaders.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.
  * Update lintian overrides for renamed tags.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 17 Jun 2022 11:24:50 +0100

libtap-harness-archive-perl (0.18-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Tue, 14 Jun 2022 14:41:32 +0200

libtap-harness-archive-perl (0.18-1) unstable; urgency=medium

  * New upstream release.
  * Update Upstream-Contact in debian/copyright.
  * Bump debhelper compatibility level to 9.

 -- gregor herrmann <gregoa@debian.org>  Sun, 29 Nov 2015 14:40:16 +0100

libtap-harness-archive-perl (0.17-1) unstable; urgency=medium

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * debian/control: update Module::Build dependency.
  * New upstream release.
  * Update (build) dependencies.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 3.9.6.
  * Mark package as autopkgtest-able.
  * Add a lintian override for a spelling-error-in-manpage warning.

 -- gregor herrmann <gregoa@debian.org>  Fri, 29 May 2015 20:52:48 +0200

libtap-harness-archive-perl (0.15-1) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * New upstream release.
    Fixes "FTBFS with perl 5.18: test failures"
    (Closes: #711257)
  * debian/copyright: switch formatting to Copyright-Format 1.0.
  * Update years of packaging copyright.
  * Set Standards-Version to 3.9.4 (no further changes).
  * Build-Depend on Module::Build 0.40.

 -- gregor herrmann <gregoa@debian.org>  Fri, 07 Jun 2013 23:18:37 +0200

libtap-harness-archive-perl (0.14-1) unstable; urgency=low

  [ Alan Haggai Alavi ]
  * Initial Release. (closes: #584077)

 -- gregor herrmann <gregoa@debian.org>  Mon, 05 Sep 2011 00:10:25 +0200
